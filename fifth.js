const person = [
    {name: 'John', age: 17},
    {name: 'Lisa', age: 17}
]


function compareAge() {
    if(person[0].age > person[1].age) {
       console.log("Person " + person[0].name + " is older than " + person[1].name);
    }else if(person[0].age < person[1].age) {
        console.log("Person " + person[0].name + " is younger than " + person[1].name)
    }else {
        console.log("Person " + person[0].name + " is the same age as " + person[1].name)
    }
    
}
compareAge();

