const {Weather} = require('./Weather')
const {Country} = require('./Country')

class City{
    constructor(name) {
        this.name = name
        this.weather = new Weather()
    }

    async setWeather(){
        await this.weather.setWeather(this.name)
    }
    async setForecast(cityName) {
        await this.weather.setForecast(this.name)
    }
    
}

module.exports = {City}