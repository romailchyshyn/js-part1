const {City} = require ('./City')
const {Weather} = require('./Weather')
class Country {
    constructor(name, cities = []) {
        this.name = name
        this.cities = cities
        
    }
    addCityToCountry(name) {
        this.cities.push(name)
    }
    deleteCityFromCountyByName(name) {
        const index =  this.cities.findIndex(city => city === name);
        this.cities.splice(index,1)
    }
    

}

module.exports = {Country}


