const {City} = require('./City')
const {Capital} = require('./Capital')
const {Country} = require('./Country')

let England = new Country('England')

let EnglandCityLondon = new City('London')
England.addCityToCountry(EnglandCityLondon)
EnglandCityLondon.setWeather().then(() => {console.log(EnglandCityLondon)}).catch((error) => {console.log(error)})

let EnglandCityManchester = new City('Manchester')
England.addCityToCountry(EnglandCityManchester)
EnglandCityManchester.setWeather().then(() => {console.log(EnglandCityManchester)}).catch((error) => {console.log(error)})

let EnglandCityLiverpool = new City('Liverpool')
England.addCityToCountry(EnglandCityLiverpool)
EnglandCityLiverpool.setWeather().then(() => {console.log(EnglandCityLiverpool)}).catch((error) => {console.log(error)})

let EnglandCitySheffield = new City ('Sheffield')
England.addCityToCountry(EnglandCitySheffield)
EnglandCitySheffield.setWeather().then(() => {console.log(EnglandCitySheffield)}).catch((error) => {console.log(error)})

let EnglandCityBirmingham = new City ('Birmingham')
England.addCityToCountry(EnglandCityBirmingham)
EnglandCityBirmingham.setWeather().then(() => {console.log(EnglandCityBirmingham)}).catch((error) => {console.log(error)})

let EnglandCityLester = new City ('Lester')
England.addCityToCountry(EnglandCityLester)
EnglandCityLester.setWeather().then(() => {console.log(EnglandCityLester)}).catch((error) => {console.log(error)})

let EnglandCityBristol = new City ('Bristol')
England.addCityToCountry(EnglandCityBristol)
EnglandCityBristol.setWeather().then(() => {console.log(EnglandCityBristol)}).catch((error) => {console.log(error)})

let EnglandCityPortsmouth = new City ('Portsmouth')
England.addCityToCountry(EnglandCityPortsmouth)
EnglandCityPortsmouth.setWeather().then(() => {console.log(EnglandCityPortsmouth)}).catch((error) => {console.log(error)})

let EnglandCitySouthampton = new City ('Southampton')
England.addCityToCountry(EnglandCitySouthampton)
EnglandCitySouthampton.setWeather().then(() => {console.log(EnglandCitySouthampton)}).catch((error) => {console.log(error)})

let EnglandCityWolverhampton = new City ('Wolverhampton')
England.addCityToCountry(EnglandCityWolverhampton)
EnglandCityWolverhampton.setWeather().then(() => {console.log(EnglandCityWolverhampton)}).catch((error) => {console.log(error)})





